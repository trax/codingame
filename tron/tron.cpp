#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <list>
#include <cstdint>
#include <set>

using std::uint32_t;

struct Coor {
    int x;
    int y;
    static const Coor Up;
    static const Coor Down;
    static const Coor Left;
    static const Coor Right;

    Coor ():x(0), y(0){}
    Coor (const int x,
          const int y):x(x),
                       y(y){}
    Coor operator+(const Coor &coor) const{
        return {x+coor.x, y+coor.y};
    }

    Coor operator-(const Coor &coor) const{
        return {x-coor.x, y-coor.y};
    }

    bool operator==(const Coor &coor) const{
        return ((x == coor.x) && (y == coor.y));
    }

};

const Coor Coor::Up    = Coor( 0,-1);
const Coor Coor::Down  = Coor( 0, 1);
const Coor Coor::Left  = Coor(-1, 0);
const Coor Coor::Right = Coor( 1, 0);

std::ostream &operator<<(std::ostream &os, const Coor &c) { 
    if (c == Coor::Up){
        return os << "UP";
    }else if (c == Coor::Down){
        return os << "DOWN";
    } else if (c == Coor::Left){
        return os << "LEFT";
    } else if (c == Coor::Right){
        return os << "RIGHT";
    } else {
        return os << "(" << c.x << ";" << c.y << ")";
    }
}


class Tron{
public:
    using PlayerID = int;
    using Players = std::vector<std::list<Coor> >;
    using Grid = std::vector <bool> ;
    using Grids = std::list <Grid>;
    using Heads = std::vector<Coor>;

    // first == score, second == #number of heads
    using Scores = std::vector<std::pair<int, int> >; 
    
private:
    const int width;
    const int height;
    Grids grids;
    Players players;
    Heads heads;
    const std::vector<Coor> around4;
    Coor currentPosition;
    Coor previousPosition;
    PlayerID myID;
    
public:
    Tron (const int width, const int height);
    void set (const uint32_t p);
    void set(const Coor coor,
             bool value = false);
    bool get(const Coor coor) const;

    void setCurrentPosition(const Coor coor);
    Coor getCurrentPosition() const;
    
    bool isIn (const Coor coor) const;

    bool isFree (const Coor coor) const;

    void take (const Coor coor);

    void addMove (const PlayerID playerID,
                  const Coor coor);

    void removePlayer (const PlayerID playerID);

    void snapshot ();
    void pop();
    std::pair<int, int> measureConnexSpace(const Coor coor);

    Coor moreSpace(const Coor coor);

    void next_read();
    Coor next_move();
    void show() const;
    void show_color() const;
};

Tron::Tron (const int width, const int height) :
    width(width),
    height(height),
    players(4),
    heads(3),
    around4({Coor::Up, Coor::Down, Coor::Right, Coor::Left}),
    currentPosition({0,0}),
    previousPosition({0,0}),
    myID(0)
{
    Grid grid (width * height, true);
    grids.push_back(grid);

}

bool Tron::isIn(const Coor coor) const{
    if (coor.x >= 0 && coor.y >= 0 &&
        coor.x < width && coor.y < height){
        return true;
    } else {
        return false;
    }
}


void Tron::set (const Coor coor, bool value){
    if (isIn(coor)){
        grids.back()[coor.y * width + coor.x] = value;
    }
}

bool Tron::get (const Coor coor) const{
    if(isIn (coor)){
        return grids.back()[coor.y * width + coor.x];
    } else {
        return false;
    }
}

void Tron::setCurrentPosition(const Coor coor){
    currentPosition = coor;
    take(currentPosition);
}

Coor Tron::getCurrentPosition() const{
    return currentPosition;
}

bool Tron::isFree(const Coor coor) const{
    return get(coor);
}

void Tron::set (const uint32_t p) {
    if (p < grids.back().size()) {
        grids.back()[p] = false;
    }
}

void Tron::take (const Coor coor) {
    set (coor, false);
}

void Tron::addMove (const PlayerID playerID,
                    const Coor coor) {
    players[playerID].push_back(coor);
    take(coor);
}

void Tron::removePlayer (const PlayerID playerID){
    auto &player = players[playerID];
    for (const auto c : player){
        set(c, true);
    }
    player.clear();
}

void Tron::snapshot () {
    grids.push_back(grids.back());
}

void Tron::pop () {
    grids.pop_back();
}


std::pair<int, int> Tron::measureConnexSpace(const Coor coor){
    int score = 0;
    snapshot();

    std::list<Coor> freeZone;
    freeZone.push_back(coor);
    take(coor);

    std::vector<bool> headsCount (4, true);
    for (const auto c : freeZone){
        for (const auto move : around4){
            auto p = c + move;
            if (isFree(p)){
                take(p);
                score++;
                freeZone.push_back(p);
            } else {
                int i = 0;
                for (const auto head : heads) {
                    if (head == p){
                        headsCount[i] = false;
                    }
                    i++;
                }
            }
        }
        
    }

    pop();
    int headSum = 0;
    for (const auto h : headsCount){
        if(!h){
            headSum++;
        }
    }
    return {score, headSum};
}

Coor Tron::moreSpace(const Coor coor){
    Coor bestDirection = Coor::Up;
    int  bestScore = -1;
    for (auto move : around4){
        auto c = coor + move;
        if (isFree(c)) {
            auto score = measureConnexSpace(c);
            auto t = score.first/(score.second+1);
            std::cerr << "score " << t << " " << score.first << " " << score.second << " ";
            if (t > bestScore){
                bestDirection = move;
                bestScore = t;
            }
            
        } 
        std::cerr << move << " " << c << " "
                  << (isFree(c)?"true":"false") 
                  << "(" << bestDirection << ":" << ")" 
                  << std::endl;
    }

    return bestDirection;
}

void Tron::next_read () {
    int N; // total number of players (2 to 4).
    int P; // your player number (0 to 3).
    std::cin >> N >> P; std::cin.ignore();
    myID = P;
    for (int i = 0; i < N; i++) {
        int X0; // starting X coordinate of lightcycle (or -1)
        int Y0; // starting Y coordinate of lightcycle (or -1)
        int X1; // starting X coordinate of lightcycle (can be the same as X0 if you play before this player)
        int Y1; // starting Y coordinate of lightcycle (can be the same as Y0 if you play before this player)
        std::cin >> X0 >> Y0 >> X1 >> Y1; std::cin.ignore();
        if (X0 == -1) {
            removePlayer(i);
        } else {
            if(i == P){
                currentPosition = {X1, Y1};
            } else {
                heads[i] = {X1, Y1};
            }
            addMove(i, {X1, Y1});
        }
    }
    std::cout << next_move() << std::endl;
            
}

Coor Tron::next_move(){

    auto move = moreSpace(currentPosition);
    currentPosition = currentPosition + move;

    return move;
}


void Tron::show() const {
    int line = 0;
    for (auto v : grids.back()){
        std::cerr << ((v)?". ":"# ");
        line++;
        if(line == width){
            std::cerr << std::endl;
            line = 0;
        }
    }
    std::cerr << std::endl;
}

void Tron::show_color() const {
    int line = 0;
    std::vector<char> grid (width * height, '.');
    std::vector<char> pions = {'#', '@', '~', '+'};
    
    int playerID = 0;
    for (const auto p : players){
        char pion = pions [playerID++];
        for (const auto move : p){
            grid[move.x + move.y*width] = pion;
        }
    }
    
    for (auto v : grid){
        std::cerr << v;
        line++;
        if(line == width){
            std::cerr << std::endl;
            line = 0;
        }
    }
    std::cerr << std::endl;
}


void oneVsOne () {
    Tron tron (30, 20);
    Tron tron0 (30, 20);
    Tron tron1 (30, 20);
    
    tron0.setCurrentPosition({3,2});
    tron1.setCurrentPosition({17,9});

    tron.set(tron0.getCurrentPosition());
    tron.set(tron1.getCurrentPosition());
    tron.show();
    bool loose = false;
    for (int i = 0 ; loose == false ; i++) {
        std::cout << i << std::endl;
        auto nextPosition0 = tron0.next_move();
        auto nextPosition1 = tron1.next_move();
        
        if(nextPosition0 == nextPosition1){
            std::cout << "draw" << std::endl;
            return;
        }

        if (!tron.isFree(nextPosition0)){
            std::cout << "0 looses" << std::endl;
            loose = true;
        }
        if (!tron.isFree(nextPosition1)){
            std::cout << "1 looses" << std::endl;
            loose = true;
        }
        if(loose) {
            return;
        }
        tron0.setCurrentPosition(nextPosition0);
        tron1.setCurrentPosition(nextPosition1);

        tron.addMove(0, nextPosition0);
        tron.addMove(1, nextPosition1);
        tron.show_color();
    }
        
    return;
}

void testFilling () {
    Tron tron  (30, 20);

    tron.setCurrentPosition({7,4});
    std::string initGrid = 
        "      ...                     "
        "      . .                     "
        "      . .                     "
        "      . .                     "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              "
        "                              ";

    
    int i = 0;
    for (const auto p : initGrid) {
        if (p != ' '){
            tron.set (i);
        }
        i++;
    }

    tron.show();

    for (int i = 0 ; i < 1 ; i++) {
        std::cerr << i << " " << tron.getCurrentPosition() << std::endl;
        auto move = tron.next_move();
        std::cerr << "moving " << move << " to " << tron.getCurrentPosition() << std::endl;
        tron.show();
    }

    tron.show();
}

int main() {
    

    Tron tron (30, 20);
    bool test = false;
    if (test) {
        testFilling();
        return 0;
    }
    
    while (1) {
        tron.next_read();
    }

    return 0;
}


